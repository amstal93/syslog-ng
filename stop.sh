#!/bin/bash

# Find out where we're running from
script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

if [ ! "$(pwd)" == "${script_dir}" ]; then
  echo "Not allowed. First execute:"
  echo "cd ${script_dir}"
  echo " Then try again."
  exit 1
fi

# shellcheck disable=SC1090
source "${script_dir}"/repo_config.txt

# stop the container
echo -n "Stopping : "
# shellcheck disable=SC2154
docker stop "${container}"
# throw the container away so we can re-start it
echo -n "Removing : "
docker rm "${container}"
