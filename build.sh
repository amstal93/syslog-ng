#!/bin/bash

# Find out where we're running from
script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

if [ $# -eq 1 ]; then
  DOCKERFILE=$1
else
  echo "Need name of Dockerfile"
  exit 1
fi

if [ ! -f "${DOCKERFILE}" ]; then
  echo "** File ${DOCKERFILE} not found."
  echo "Execute this script from within ${script_dir}"
  exit 1
fi

# shellcheck disable=SC1090
source "${script_dir}/repo_config.txt"


cp "${DOCKERFILE}" Dockerfile

# build a local image
# shellcheck disable=SC2154
docker build --rm -t "${image}" . || exit 1

# copy the configuration file to a user-managed location.
if [ ! -f "${HOME}/.config/docker/syslog-ng.conf" ]; then
  mkdir -p "${HOME}/.config/docker/"
  cp "${script_dir}/container/default.conf" "${HOME}/.config/docker/syslog-ng.conf"
fi

# create a user configuration pointing at the storage location
# shellcheck disable=SC2154
if [ ! -f "${HOME}/.config/docker/${container}-store.txt" ]; then
  mkdir -p "${HOME}/.config/docker/"
  echo "store=${store}" > "${HOME}/.config/docker/${container}-store.txt"
fi

# remember the distro
echo "${DOCKERFILE}" | cut -d'.' -f 2 > "${HOME}/.config/docker/${container}-distro.txt"

rm Dockerfile
